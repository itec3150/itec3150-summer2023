import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.List;
import java.util.Scanner;

public class CityReader {

    /*
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(new File("cities.txt"));
        while (sc.hasNextLine()) {
            System.out.println(sc.nextLine());
        }
    }*/

    public static void main(String[] args) throws Exception {
        List<String> lines = FileUtils.readLines(new File("cities.txt"), "UTF-8");
        System.out.println(lines.get(0));
        System.out.println(lines.get(lines.size()-1));
    }

}
