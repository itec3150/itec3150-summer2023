import org.apache.commons.io.IOUtils;

import java.net.URL;
import java.util.PriorityQueue;
import java.util.Queue;

public class StockPriceAnalysis {

    public static void main(String[] args) {
        printFiveLowestStockPrice("AMZN");
    }

    public static void printFiveLowestStockPrice(String ticker) {
        Queue<Double> prices = new PriorityQueue<>();
        try {
            URL url = new URL("https://gist.githubusercontent.com/tacksoo/7d5f3a08d4d31335b5e2d8cb91630153/raw/8e0250840affeac9f33edd65d518be06a78b5145/amazon.csv");
            String str = IOUtils.toString(url.openStream());
            String[] lines = str.split("\n");
            for (int i = 0; i < lines.length; i++) {
                String[] cols = lines[i].split(",");
                prices.add(Double.parseDouble(cols[2]));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 5; i++) { // print out five lowest prices
            System.out.println(prices.poll());
        }
    }
}
