import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.List;
import java.util.TreeMap;

public class GunAnalysis {
    public static void main(String[] args) throws Exception {
        File file = new File("gun.csv");
        List<String> lines = FileUtils.readLines(file,"UTF-8");
        TreeMap<String,Integer> counter = new TreeMap<>();
        for (int i = 1; i < lines.size(); i++) {
            String line = lines.get(i);
            String[] cols = line.split(",");
            String state = cols[3];
            if (counter.containsKey(state)) {
                int count = counter.get(state);
                counter.put(state,count+1);
            } else {
                counter.put(state,1);
            }
        }
        System.out.println(counter);
    }
}
