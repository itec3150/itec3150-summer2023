import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class GameObjectReader {

    public static void main(String[] args) {
        // deserialize "games.json" into an array of game objects
        ObjectMapper mapper = new ObjectMapper();
        try {
            Game[] games = mapper.readValue(new File("games.json"), Game[].class);
            System.out.println(Arrays.toString(games));
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
