import java.util.PriorityQueue;

public class PriorityQueueExample {

    public static void main(String[] args) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        pq.add(5);
        pq.add(7);
        pq.add(-3);
        pq.add(-42);
        pq.add(10000);
        pq.add(5000);
        pq.add(-678);
        System.out.println(pq.size());
        System.out.println(pq.peek());
        System.out.println(pq.peek());
        System.out.println(pq.poll());
        System.out.println(pq);
        // how can a PriorityQueue be used for sorting?
        while(!pq.isEmpty()) {
            System.out.println(pq.poll());
        }
    }
}
