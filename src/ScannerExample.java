import java.util.Scanner;

public class ScannerExample {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter your name: ");
        String name = keyboard.nextLine();
        System.out.println("What is your favorite integer: ");
        String fav = keyboard.nextLine();
        int favNum = Integer.parseInt(fav);
        System.out.println(name + ":" + favNum);
    }
}
