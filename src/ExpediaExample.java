import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ExpediaExample {

    public static void main(String[] args) {
        System.setProperty("webdrive.chrome.driver","chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com");
        WebElement location = driver.findElement(By.name("q"));
        location.click();
        location.click();
        location.sendKeys("Brooklyn");
        location.submit();
    }

}
