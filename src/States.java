import java.io.File;
import java.util.HashMap;
import java.util.Scanner;

public class States {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(new File("states.txt"));
        HashMap<String, String> states = new HashMap<String, String>();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] columns = line.split(",");
            String stateName = columns[0];
            String abbrev = columns[1];
            states.put(abbrev, stateName);
        }
        //The size of the HashMap is 50
        //The abbreviation GA is for Georgia
        System.out.println("The size of the HashMap is " + states.size());
        System.out.println("The abbreviation GA is for " + states.get("GA"));
        System.out.println(states.containsKey("PR"));

        System.out.println(states.remove("CA"));
        
        // what is a set?

    }
}
