import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.Stack;

public class Balanced {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        while (true) {
            System.out.print("Please enter a sentence: ");
            String line = keyboard.nextLine();
            if (isBalanced(line)) {
                System.out.println("The sentence is balanced.");
            } else {
                System.out.println("The sentence is not balanced.");
            }
            System.out.print("Type enter to continue or quit to exit: ");
            line = keyboard.nextLine();
            if (line.equals("quit")) break;
        }
    }
    /*
    balanced string algorithm (stack version)
        1. if you see a '(', push it to the stack
        2. if you see a ')', pop from stack
           a. if this causes a EmptyStackException then
              you know the string is unbalanced
        3. repeat this until end of string
        4. count size of stack
           a. if size of stack is 0, string is balanced
           b. if size of stack is not 0, string is unbalanced
     */
    public static boolean isBalanced(String line) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '(') {
                stack.push('(');
            }
            if (line.charAt(i) == ')') {
                try {
                    stack.pop(); // try catch and in the catch return false
                } catch (EmptyStackException e) {
                    return false;
                }
            }
        }
        return stack.size() == 0 ? true : false;
    }


}
