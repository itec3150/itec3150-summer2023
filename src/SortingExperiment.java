import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SortingExperiment {

    public static void main(String[] args) {

        List<Integer> nums = new ArrayList<>();
        Random r = new Random();
        for (int i = 0; i < 16000000; i++) {
            nums.add(i);
        }
        Collections.shuffle(nums);

        long start = System.currentTimeMillis();
        //Collections.sort(nums);
        long end = System.currentTimeMillis();
        //System.out.println(end-start);

        int[] n = new int[80000];
        for (int i = 0; i < 80000; i++) {
            n[i] = r.nextInt();
        }
        start = System.currentTimeMillis();
        bubbleSort(n);
        end = System.currentTimeMillis();
        System.out.println(end - start);

        //  how long would it take to sort 2 million number
        //  c. 25000
        //  m. 1004
        //  j. 800
        //  ja. 750
        //  k. 1200
        //  h. 1500



    }

    public static void bubbleSort(int[] a) {
        boolean sorted = false;
        int temp;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < a.length - 1; i++) {
                if (a[i] > a[i+1]) {
                    temp = a[i];
                    a[i] = a[i+1];
                    a[i+1] = temp;
                    sorted = false;
                }
            }
        }
    }
}
