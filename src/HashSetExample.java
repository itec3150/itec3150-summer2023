import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class HashSetExample {
    public static void main(String[] args) {
        Set<String> names = new TreeSet<String>();
        names.add("bob");
        names.add("patrick");
        names.add("peter");
        names.add("sarah");
        names.add("bob");
        System.out.println(names);
    }
}
