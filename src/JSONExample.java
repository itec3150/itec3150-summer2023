import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URL;

public class JSONExample {

    public static void main(String[] args) throws IOException {
        URL url = new URL("https://www.cheapshark.com/api/1.0/deals?storeID=1");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(url);
        String firstGame = root.get(0).get("title").asText();
        double salePrice = root.get(0).get("salePrice").asDouble();
        //System.out.println("Buy: " + firstGame + " $" + salePrice);

        // challenge: print out all games in the JSON file
        for (int i = 0; i < root.size(); i++) {
            String title = root.get(i).get("title").asText();
            double price = root.get(i).get("salePrice").asDouble();
            System.out.println(title + ":   $" + price);
        }
    }

}
