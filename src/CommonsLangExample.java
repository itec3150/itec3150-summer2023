import org.apache.commons.lang3.StringUtils;

public class CommonsLangExample {
    public static void main(String[] args) {
        // install Apache Commons Lang library into your project
        // and use StringUtils
        boolean b = StringUtils.containsAny("hello world lol", "lol");
        System.out.println(b);
    }
}
