import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Location {
    private String name;
    private double latitude;
    private double longitude;
    private String gridId;
    private int gridX;
    private int gridY;
    private double temperature;
    private String forecast;

    public Location(String name, double latitude, double longitude, String gridId, int gridX, int gridY) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.gridId = gridId;
        this.gridX = gridX;
        this.gridY = gridY;
    }

    public void printCurrentWeather() {
        String address = "https://api.weather.gov/gridpoints/" +
                gridId + "/" + gridX + "," + gridY + "/" + "forecast/hourly";
        try {
            URL url = new URL(address);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(url);
            double temp = root.get("properties").get("periods").get(0).get("temperature").asDouble();
            System.out.println(temp);
        } catch(MalformedURLException e) {
            System.out.println("Hey, your URL is messed up!");
            e.printStackTrace();
        } catch(IOException e) {
            System.out.println("Exception detected on parsing JSON!");
            e.printStackTrace();
        }
    }

    public void setForecast() {
        String address = "https://api.weather.gov/gridpoints/" +
            gridId + "/" + gridX + "," + gridY + "/" + "forecast";
        try {
            URL url = new URL(address);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(url);
            double temp = root.get("properties").get("periods").get(0).get("temperature").asDouble();
            String forecast = root.get("properties").get("periods").get(0).get("detailedForecast").asText();
            this.forecast = forecast;
            //System.out.println(forecast);
            //System.out.println(temp);
            this.temperature = temp;
        } catch(MalformedURLException e) {
            System.out.println("Hey, your URL is messed up!");
            e.printStackTrace();
        } catch(IOException e) {
            System.out.println("Exception detected on parsing JSON!");
            e.printStackTrace();
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getGridId() {
        return gridId;
    }

    public void setGridId(String gridId) {
        this.gridId = gridId;
    }

    public int getGridX() {
        return gridX;
    }

    public void setGridX(int gridX) {
        this.gridX = gridX;
    }

    public int getGridY() {
        return gridY;
    }

    public void setGridY(int gridY) {
        this.gridY = gridY;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getForecast() {
        return forecast;
    }

    public void setForecast(String forecast) {
        this.forecast = forecast;
    }
}
