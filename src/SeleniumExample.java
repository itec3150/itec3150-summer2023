import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

public class SeleniumExample {

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://news.ycombinator.com/");
        List<WebElement> elements = driver.findElements(By.className("titleline"));
        for (int i = 0; i < elements.size(); i++) {
            String headLine = elements.get(i).getText();
            System.out.println(headLine);
        }

        driver.get("https://www.slashdot.org/");
        elements = driver.findElements(By.className("story"));
        for (int i = 0; i < elements.size(); i++) {
            String story = elements.get(i).getText();
            System.out.println(story);
        }

        //driver.close();
    }
}
