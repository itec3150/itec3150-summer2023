import org.apache.commons.io.IOUtils;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HighPoint {

    public static void main(String[] args) {
        Map<String,Integer> highPoints = getStateHighPoint("https://gist.githubusercontent.com/tacksoo/32e5badc23f992b9dce6c8a65c456edf/raw/0105d13f86f5b5741d1efd00a382a43e3b5c8cd2/highpoints.csv");
        System.out.println(highPoints.get("Georgia"));
    }

    public static HashMap<String,Integer> getStateHighPoint(String link) {
        HashMap<String,Integer> highPoints = new HashMap<>();
        try {
            URL url = new URL(link);
            String str = IOUtils.toString(url.openStream());
            String[] lines = str.split("\n");
            for (int i = 0; i < lines.length; i++) {
                String[] cols = lines[i].split(",");
                highPoints.put(cols[0], Integer.parseInt(cols[2]));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return highPoints;
    }

}
