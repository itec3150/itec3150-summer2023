import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Scores {

    public static void main(String[] args) throws Exception {
//        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(new Comparator<Integer>() {
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                return -o1.compareTo(o2);
//            }
//        });
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Collections.reverseOrder());
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();

        Scanner keyboard = new Scanner(System.in);

        while(true) {
            System.out.print("Please enter a number: ");
            int num = Integer.parseInt(keyboard.nextLine());
            maxHeap.add(num);
            minHeap.add(num);
            System.out.println("The biggest number was: " + maxHeap.peek());
            System.out.println("The smallest number was: " + minHeap.peek());
        }
    }
}
