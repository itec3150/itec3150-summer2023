import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.PrintWriter;

public class FileWritingExample {

    public static void main(String[] args) throws Exception {
        PrintWriter pw = new PrintWriter("heroes.txt");
        pw.println("hello world!");
        pw.println("programming (can be) fun!!!");
        pw.println("go itec3150!");
        pw.close();
        String str = "night classes in the summer are the best!";
        FileUtils.writeStringToFile(new File("best.txt"),str,"UTF-8");
    }
}
