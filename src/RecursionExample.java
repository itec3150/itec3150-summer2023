import java.util.Stack;

public class RecursionExample {
    public static void main(String[] args) {
        System.out.println(factorial(20));
    }

    public static long factorial(long n) {
        Stack<Long> stack = new Stack<>();
        stack.push(n);
        long result = 1;
        while (!stack.isEmpty()) {
            Long top = stack.pop();
            result = result * top;
            if (top-1 != 0) {
                stack.push(top - 1);
            }
        }
        return result;
    }
}
