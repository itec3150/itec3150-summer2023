import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class Heroes {

    public static void main(String[] args) {
        //get a map of heroes from users
        TreeMap<String,String> heroes = getHeroes();
        //write treemap to a textfile
        writeHeroesToFile(heroes);
    }

    public static void writeHeroesToFile(TreeMap<String,String> heroes) {
        try {
            Set<String> names = heroes.keySet();
            PrintWriter pw = new PrintWriter("heroes.txt");
            for (String name : names) {
                pw.println(name + ":" + heroes.get(name));
            }
            pw.close();
        } catch(IOException e) {
            throw new RuntimeException("writing to file failed!");
        }
    }


    public static TreeMap<String,String> getHeroes() {
        TreeMap<String,String> heroes = new TreeMap<>();
        try {
            Scanner keyboard = new Scanner(System.in);
            String name = "";
            String home = "";
            while(true) {
                System.out.println("Please enter name of hero: ");
                name = keyboard.nextLine();
                System.out.println("Please enter home town of hero: ");
                home = keyboard.nextLine();
                heroes.put(name,home);  // insert the pair to the treemap
                System.out.println("Type quit to end or enter to continue: ");
                String exit = keyboard.nextLine();
                if (exit.equals("quit")) {
                    break;
                }
            }
        } catch(Exception io) {
            throw new RuntimeException("Something went wrong with Scanner!");
        }
        return heroes;
    }
}
