public class HashCodeExample {

    public static void main(String[] args) {
        // example of a hash code
        System.out.println("Chidiebube".hashCode());
        System.out.println("Wallace".hashCode());
        System.out.println("Anthony".hashCode());
        String awesomeGuy = "Anthony";
        System.out.println(awesomeGuy.hashCode());
        System.out.println("dr.im".hashCode());

        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);


    }
}
