import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class BiMapExample {

    public static void main(String[] args) {
        HashBiMap<String,String> person = HashBiMap.create();
        person.put("chidiebube","12345");
        person.put("alvin","23456");
        person.put("noah","34567");
        BiMap<String,String> inversePersons = person.inverse();
        System.out.println(inversePersons.get("12345"));

    }
}
