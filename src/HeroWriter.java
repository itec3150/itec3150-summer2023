import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HeroWriter {
    public static void main(String[] args) {
        Hero you = new Hero("You",22,"Lawrenceville");
        List<Hero> heroes = new ArrayList<>();
        heroes.add(you);
        heroes.add(new Hero("dr.im",21, "Lawrenceville"));
        heroes.add(new Hero("superman",42, "metropolis"));
        heroes.add(new Hero("one punch man", 25, "tokyo"));
        ObjectMapper mapper = new ObjectMapper();
        try {
            String str = mapper.writeValueAsString(heroes);
            String youStr = mapper.writeValueAsString(you);
            System.out.println(str);
            FileUtils.writeStringToFile(new File("you.json"), youStr, "UTF-8");
            FileUtils.writeStringToFile(new File("heroes.json"), str, "UTF-8");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
