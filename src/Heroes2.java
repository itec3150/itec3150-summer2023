import java.sql.SQLOutput;
import java.util.Scanner;
import java.util.TreeSet;

public class Heroes2 {

    public static void main(String[] args) {
        TreeSet<Hero> heroes = new TreeSet<>();
        while (true) {
            Scanner keyboard = new Scanner(System.in);
            System.out.print("Please enter name of hero: ");
            String name = keyboard.nextLine();
            System.out.print("Please enter age of hero: ");
            int age = Integer.parseInt(keyboard.nextLine());
            System.out.print("Please enter home town of hero: ");
            String home = keyboard.nextLine();
            // create a hero object based on user input
            Hero hero = new Hero(name, age, home);
            heroes.add(hero);
            System.out.println("Currently, we have " + heroes);
            System.out.print("Type quit to end or enter to continue: ");
            String answer = keyboard.nextLine();
            if (answer.equals("quit")) break;
        }
    }
}
