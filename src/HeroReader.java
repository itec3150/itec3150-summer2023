import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HeroReader {

    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            //String str = FileUtils.readFileToString(new File("you.json"), "UTF-8");
            Hero hero = mapper.readValue(new File("you.json"), Hero.class); //deserialize into Hero object
            System.out.println(hero);
            Hero[] heroes = mapper.readValue(new File("heroes.json"), Hero[].class);
            System.out.println(Arrays.toString(heroes));
        } catch(IOException e) {
            e.printStackTrace();
        }

    }
}
