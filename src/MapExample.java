import java.util.*;

public class MapExample {

    public static void main(String[] args) {
        // (ssn, student name)
        Map<Integer, String> names = new TreeMap<Integer, String>();
        names.put(1234,"Superman");
        names.put(2345,"Wonder Woman");
        names.put(9876,"One Punch Man");
        names.put(3456,"Batman");
        names.put(5678,"Catwoman");

        System.out.println(names.get(9876));

        // this is how you can go over all entries in a HashMap
        Set<Integer> ssns = names.keySet();
        for(int ssn: ssns) {
            System.out.println(ssn + ":" + names.get(ssn));
        }

        // given a string such as "georgia"
        // what is the first unique letter?
    }
}
