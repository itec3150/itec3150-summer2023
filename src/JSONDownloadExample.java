import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.net.URL;
import java.util.List;

public class JSONDownloadExample {

    public static void main(String[] args) throws Exception {
        // download from https://api.weather.gov/points/{latitude},{longitude}
        // to get  office, grid X, grid Y
        URL url = new URL("https://api.weather.gov/points/37.8081988,-122.4244214");
        String str = IOUtils.toString(url.openStream());
        System.out.println(str);
        // JSON parsing
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(str);
        String gridId = root.get("properties").get("gridId").asText();
        int gridX = root.get("properties").get("gridX").asInt();
        int gridY = root.get("properties").get("gridY").asInt();
        System.out.println(gridId + " " + gridX + " " + gridY);

        //Location ggc = new Location("GGC",37.80,-122.42,"FFC",64,98);
        //ggc.setForecast();

        List<String> lines = FileUtils.readLines(new File("locations.csv"),"UTF-8");
        for (String line: lines) {
            String[] cols = line.split(",");
            String name = cols[0];
            double latitude = Double.parseDouble(cols[1]);
            double longitude = Double.parseDouble(cols[2]);
            String gID = cols[3];
            int gX = Integer.parseInt(cols[4]);
            int gY = Integer.parseInt(cols[5]);
            Location location  = new Location(name,latitude,longitude,gID,gX,gY);
            location.setForecast();
            System.out.println(name);
            System.out.println("Currently: ");
            location.printCurrentWeather();
            System.out.println(location.getTemperature() + ":  " + location.getForecast());
        }
    }
}
