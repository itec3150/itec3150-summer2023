import java.util.Comparator;
import java.util.TreeSet;

public class AmericanState implements Comparable<AmericanState> {
    private String abbreviation;
    private String fullName;
    private int foundYear;

    public AmericanState(String abbreviation, String fullName, int foundYear) {
        this.abbreviation = abbreviation;
        this.fullName = fullName;
        this.foundYear = foundYear;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getFoundYear() {
        return foundYear;
    }

    public void setFoundYear(int foundYear) {
        this.foundYear = foundYear;
    }

    @Override
    public String toString() {
        return "AmericanState{" +
                "abbreviation='" + abbreviation + '\'' +
                ", fullName='" + fullName + '\'' +
                ", foundYear='" + foundYear + '\'' +
                '}';
    }

    public static void main(String[] args) {
        AmericanState georgia = new AmericanState("GA", "Georgia", 1788);
        AmericanState tennessee = new AmericanState("TN", "Tennessee", 1796);
        AmericanState alabama = new AmericanState("AL", "Alabama", 1819);
        AmericanState hawaii = new AmericanState("HI", "Hawaii", 1959);
        System.out.println(georgia);
        System.out.println(tennessee);
        TreeSet<AmericanState> states = new TreeSet<>();
        states.add(georgia);
        states.add(tennessee);
        states.add(alabama);
        states.add(hawaii);
        System.out.println(states);
    }

    @Override
    public int compareTo(AmericanState o) {
        //return o.getAbbreviation().compareTo(this.getAbbreviation());
        return this.getAbbreviation().compareTo(o.getAbbreviation());
        //return ((Integer) this.getFoundYear()).compareTo(o.foundYear);
    }
}
