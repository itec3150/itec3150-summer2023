import java.util.Stack;

public class BTNodeExample {

    public static void main(String[] args) {
        BTNode five = new BTNode(5, null,null);
        BTNode twentyFive = new BTNode(25,null,null);
        BTNode twenty = new BTNode(20, five, twentyFive);
        BTNode seventy = new BTNode(70,null,null);
        BTNode fifty = new BTNode(50,null,seventy);
        BTNode root = new BTNode(42, twenty, fifty);
        /*
                      42
                     /  \
                    20   50
                   /  \    \
                  5   25    70
            how do we access all nodes of a binary tree?
            answer: use recursion!
         */
        inorderStackVersion(root);
    }

    // preorder , inorder, postorder

    public static void preorderTraversal(BTNode node) {
        System.out.println(node);
        if (node.getLeftChild() != null)
            preorderTraversal(node.getLeftChild());
        if (node.getRightChild() != null)
            preorderTraversal(node.getRightChild());
    }

    public static void inorderTraversal(BTNode node) {
        if (node.getLeftChild() != null)
            inorderTraversal(node.getLeftChild());
        System.out.println(node);
        if (node.getRightChild() != null)
            inorderTraversal(node.getRightChild());
    }

    public static void postorderTraversal(BTNode node) {
        if (node.getLeftChild() != null)
            inorderTraversal(node.getLeftChild());
        if (node.getRightChild() != null)
            inorderTraversal(node.getRightChild());
        System.out.println(node);
    }

    //  )
    //  (
    //  ()
    // (())

    public static void inorderStackVersion(BTNode root) {
        Stack<BTNode> stack = new Stack<>();
        stack.push(root);
        while(!stack.isEmpty()) {
            BTNode top = stack.pop();
            if (top.getLeftChild() != null) {
                stack.push(top.getLeftChild());
            }
            System.out.println(top);
            if (top.getRightChild() != null) {
                stack.push(top.getRightChild());
            }
        }
    }

}
