import java.util.Scanner;

public class BreakContinue {

    public static void main(String[] args) {
        // continue example
        for (int i = 0; i < 10; i++) {
            if (i == 4) {
                continue;
            }
            System.out.println("hello " + i);
        }

        // break example
        Scanner scanner = new Scanner(System.in);
        for (;true;) {
            System.out.println("Please enter something nice: ");
            String str = scanner.nextLine();
            if (str.equals("not nice")) {
                break;
            }
            System.out.println(str + " is nice!");
        }
    }
}
