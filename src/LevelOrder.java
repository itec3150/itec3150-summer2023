import java.util.LinkedList;

public class LevelOrder {

    public static void main(String[] args) {
        BinaryNode seventyTwo = new BinaryNode(72, null, null);
        BinaryNode fortyOne = new BinaryNode(41, null, null);
        BinaryNode thirtyThree = new BinaryNode(33, null, null);
        BinaryNode sixty = new BinaryNode(60, null, seventyTwo);
        BinaryNode thirtyEight = new BinaryNode(38, thirtyThree, fortyOne);
        BinaryNode twentyOne = new BinaryNode(21, null, null);
        BinaryNode fifty = new BinaryNode(50, null, sixty);
        BinaryNode twentySeven = new BinaryNode(27, twentyOne, thirtyEight);
        BinaryNode root = new BinaryNode(42, twentySeven, fifty);
        printNodesInLevelOrder(root);
    }

    public static void printNodesInLevelOrder(BinaryNode root) {
        // 1. create a queue (can be a linkedlist or arraylist)
        // 2. add the root
        // 3. while queue not empty
        //   a. remove head and print
        //   b. add children of head to queue
        LinkedList<BinaryNode> list = new LinkedList<>();
        list.add(root);
        while (!list.isEmpty()) {
            BinaryNode head = list.poll();
            System.out.print(head.getValue() + " ");
            if (head.getLeftChild() != null) {
                list.add(head.getLeftChild());
            }
            if (head.getRightChild() != null) {
                list.add(head.getRightChild());
            }
        }
    }
}
