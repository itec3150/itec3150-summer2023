import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SerializationUtils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GameObjectWriter {

    public static void main(String[] args) {
        List<Game> games = new ArrayList<>();
        Game duke = new Game("Duke Nukem 3d","PC", "3d realms", "Shooter",10,1991);
        Game truck = new Game("American Truck Simulator", "PC", "SCS Software","Simulator",10,2016);
        Game starcraft = new Game("Starcraft", "PC","Blizzard", "Realtime Strategy", 10, 1996);
        games.add(duke);
        games.add(truck);
        games.add(starcraft);

        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(games);
            FileUtils.writeStringToFile(new File("games.json"),json, "UTF-8");
        } catch(JsonProcessingException e) {
            e.printStackTrace();
            System.out.println("JSON can't be created");
        } catch(IOException e) {
            e.printStackTrace();
            System.out.println("Can't write to file");
        }
    }
}
