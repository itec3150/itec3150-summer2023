public class Billionaire implements Comparable<Billionaire> {

    // from the website: https://corgis-edu.github.io/corgis/csv/billionaires/
    // name - index: 0
    // netWorth - index: len() - 7 or 14
    // age - index : 8
    // location - index : 12


    private String name;
    private double netWorth;
    private int age;
    private String location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(double netWorth) {
        this.netWorth = netWorth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Billionaire(String name, double netWorth, int age, String location) {
        this.name = name;
        this.netWorth = netWorth;
        this.age = age;
        this.location = location;
    }

    @Override
    public String toString() {
        return "Billionaire{" +
                "name='" + name + '\'' +
                ", netWorth=" + netWorth +
                ", age=" + age +
                ", location='" + location + '\'' +
                '}';
    }

    @Override
    public int compareTo(Billionaire o) {
        return ((Double) o.getNetWorth()).compareTo(this.getNetWorth());
    }
}
