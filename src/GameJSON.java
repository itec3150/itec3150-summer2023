import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URL;

public class GameJSON {
    public static void main(String[] args) {
        printTitles("https://www.cheapshark.com/api/1.0/deals?storeID=7");
    }

    public static void printTitles(String link) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(new URL(link));
            for (int i = 0; i < root.size(); i++) {
                System.out.println(root.get(i).get("title"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
