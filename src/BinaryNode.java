public class BinaryNode {
    private int value;
    private BinaryNode leftChild;
    private BinaryNode rightChild;

    public BinaryNode(int value) {
        this.value = value;
        leftChild = null;
        rightChild = null;
    }

    public BinaryNode(int value, BinaryNode leftChild, BinaryNode rightChild)
    {
        this.value = value;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public BinaryNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(BinaryNode leftChild) {
        this.leftChild = leftChild;
    }

    public BinaryNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(BinaryNode rightChild) {
        this.rightChild = rightChild;
    }

    @Override
    public String toString() {
        return "BinaryNode: " +
                "value=" + value;
    }
}
