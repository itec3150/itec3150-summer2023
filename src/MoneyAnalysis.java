import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

public class MoneyAnalysis {

    public static void main(String[] args) throws Exception {
//        URL url = new URL("https://corgis-edu.github.io/corgis/datasets/csv/billionaires/billionaires.csv");
//        InputStream in = url.openStream();
//        String csv = IOUtils.toString(in);
//        //System.out.println(csv);
//        in.close();
        //Scanner scanner = new Scanner(new File("billys.csv"));
        List<String> lines = FileUtils.readLines(new File("billys.csv"),"UTF-8");
        //String [] lines = csv.split("\n");

        HashSet<Integer> sizes = new HashSet<>();
        for (int i = 1; i < lines.size(); i++) {
            String line = lines.get(i);
            String [] cols = line.split(",");
            sizes.add(cols.length);
        }
        System.out.println(sizes);
        //System.out.println(getBillionaires(lines, 1996));

    }

    public static TreeSet<Billionaire> getBillionaires(String[] lines, int year) {
        TreeSet<Billionaire> onePercents = new TreeSet<>();
        for (int i = 1; i < lines.length; i++) {
            String line = lines[i];
            String[] cols = line.split(",");
            if (Integer.parseInt(cols[2].substring(1,cols[2].length()-1)) == year) {  // sampled year is cols[2]
                String name = cols[0];
                double netWorth = Double.parseDouble(cols[17].substring(1,cols[17].length()-1));
                int age = Integer.parseInt(cols[8].substring(1,cols[8].length()-1));
                String location = cols[12];
                Billionaire billy = new Billionaire(name, netWorth, age, location);
                onePercents.add(billy);
            }
        }
        return onePercents;
    }

}
